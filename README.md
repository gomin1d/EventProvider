# Провайдер событий [![](https://jitpack.io/v/com.gitlab.Maxim724/EventProvider.svg)](https://jitpack.io/#com.gitlab.Maxim724/EventProvider)
Библиотека событий для Lokha

# Скромная Документация
```
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>

<dependency>
    <groupId>com.gitlab.Maxim724</groupId>
    <artifactId>EventProvider</artifactId>
    <version>724.1</version>
</dependency>
```
