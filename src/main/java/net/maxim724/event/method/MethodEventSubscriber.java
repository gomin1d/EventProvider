package net.maxim724.event.method;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.maxim724.event.api.EventSubscriber;
import net.maxim724.event.api.Priority;
import net.maxim724.event.method.engine.Agent;
import org.jetbrains.annotations.NotNull;

@ToString
@EqualsAndHashCode
public final class MethodEventSubscriber<E, L> implements EventSubscriber<E> {
    private final Class<? extends E> event;
    private final Priority priority;

    @Getter private final L listener;
    private final Agent<E, L> executor;

    public MethodEventSubscriber(@NotNull Class<? extends E> eventClass, @NotNull Priority priority, @NotNull L listener, @NotNull Agent<E, L> executor) {
        this.event = eventClass;
        this.priority = priority;

        this.listener = listener;
        this.executor = executor;
    }

    @Override
    public void handle(@NotNull E event) throws Throwable {
        executor.invoke(event, listener);
    }

    @Override
    public @NotNull Priority priority() {
        return priority;
    }
}