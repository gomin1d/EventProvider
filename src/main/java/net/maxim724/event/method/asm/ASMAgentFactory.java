package net.maxim724.event.method.asm;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import net.maxim724.event.method.engine.Agent;
import net.maxim724.event.helper.RandomHelper;
import net.maxim724.event.utilty.DefiningClassLoader;
import org.jetbrains.annotations.NotNull;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static org.objectweb.asm.Opcodes.*;

/**
 * Фабрика посредников на основе ASM
 *
 * @param <L> тип слушателя
 * @param <E> тип события
 *
 * @since 724.0
 */
public final class ASMAgentFactory<L, E> implements Agent.Factory<L, E> {
    /* Константы */
    /* В этом пакете будут созданы новые классы */
    private static final String PACKAGE = "net.maxim724.event.asm.generated";

    /* Внутреннее имя Object */
    private static final String OBJECT_INTERNAL_NAME = "java/lang/Object";
    /* Внутреннее имя Agent */
    private static final String[] AGENT_INTERNAL_NAME = new String[] { Type.getInternalName(Agent.class) };

    /* Дескриптор метода void Agent#invoke(Obj, Obj) */
    private static final String AGENT_METHOD_DESCTIPTOR = "(Ljava/lang/Object;Ljava/lang/Object;)V";

    /* Экземпляр */
    /* Загрузчик сгенерированных классов */
    private final DefiningClassLoader classloader;
    /* Сессия фабрики (Чтобы не было конфликтов с другими фабриками) */
    private final String session = RandomHelper.generateSession(8);

    /* Для идентификации посредников */
    private final AtomicInteger id = new AtomicInteger();
    /* Кэш сгенерированных посредников */
    private final LoadingCache<Method, Class<? extends Agent<L, E>>> cache;

    public ASMAgentFactory(@NotNull ClassLoader parent) {
        this.classloader = new DefiningClassLoader(parent);
        this.cache = CacheBuilder.newBuilder()
                .weakValues()
                .initialCapacity(8)
                .build(CacheLoader.from(method -> {
                    /* Проверяем аргументы, которые будут приходить извне */
                    Objects.requireNonNull(method, "method");

                    /* Получаем тип слушателя и его внутреннее имя */
                    Class<?> listener = method.getDeclaringClass();
                    String listenerName = Type.getInternalName(listener);

                    /* Получаем тип события и его внутреннее имя */
                    Class<?> event = method.getParameterTypes()[0];
                    String eventName = Type.getInternalName(event);

                    /* Генерируем внутреннее имя нового класса */
                    String className = String.format("%s.%s.%s$%s.%s$%d",
                            PACKAGE, session, listener.getSimpleName(), method.getName(), event.getSimpleName(), id.incrementAndGet());

                    /* Генерируем новый класс */
                    ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
                    cw.visit(V1_8, ACC_PUBLIC | ACC_FINAL, className.replace('.', '/'), null, OBJECT_INTERNAL_NAME, AGENT_INTERNAL_NAME);

                    /* Генерируем методы */
                    MethodVisitor mv;

                    /* Генерируем конструктор */
                    mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
                    mv.visitCode();

                    mv.visitVarInsn(ALOAD, 0);
                    mv.visitMethodInsn(INVOKESPECIAL, OBJECT_INTERNAL_NAME, "<init>", "()V", false);

                    mv.visitInsn(RETURN);

                    mv.visitMaxs(0, 0);
                    mv.visitEnd();

                    /* Генерируем метод handle */
                    mv = cw.visitMethod(ACC_PUBLIC, "invoke", AGENT_METHOD_DESCTIPTOR, null, null);
                    mv.visitCode();

                    mv.visitVarInsn(ALOAD, 1);
                    mv.visitTypeInsn(CHECKCAST, eventName);
                    mv.visitVarInsn(ALOAD, 2);
                    mv.visitTypeInsn(CHECKCAST, listenerName);

                    mv.visitInsn(SWAP);
                    mv.visitMethodInsn(INVOKEVIRTUAL, listenerName, method.getName(), Type.getMethodDescriptor(method), false);

                    mv.visitInsn(RETURN);

                    mv.visitMaxs(0, 0);
                    mv.visitEnd();

                    cw.visitEnd();

                    /* Загружаем сгенерированный класс */
                    return classloader.defineClass(className, cw.toByteArray());
                }));
    }

    public @NotNull Agent<L, E> create(@NotNull Object object, @NotNull Method method) throws IllegalAccessException, InstantiationException {
        if (!Modifier.isPublic(object.getClass().getModifiers())) {
            throw new IllegalArgumentException("Listener '" + object.getClass().getName() + "' must be public");
        }

        if (!Modifier.isPublic(method.getModifiers())) {
            throw new IllegalArgumentException("Subscriber '" + method + "' must be public");
        }

        return cache.getUnchecked(method).newInstance();
    }
}