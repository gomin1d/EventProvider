package net.maxim724.event.method.exception;

/**
 * Исключение, которое будет выкинуто в случае неудачи создания посредника
 * @since 724.0
 */
// шинда -___-
public final class SubscriberGenerationException extends RuntimeException {
    public SubscriberGenerationException(String message) {
        super(message);
    }

    public SubscriberGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}