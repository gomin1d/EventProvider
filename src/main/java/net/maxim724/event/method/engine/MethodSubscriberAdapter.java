package net.maxim724.event.method.engine;

import net.maxim724.event.api.EventProvider;
import net.maxim724.event.api.EventSubscriber;
import net.maxim724.event.api.method.Subscribe;
import net.maxim724.event.method.MethodEventSubscriber;
import net.maxim724.event.method.exception.SubscriberGenerationException;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.function.BiConsumer;

public final class MethodSubscriberAdapter<L, E> {
    /* Экземпляр */
    private final Class<E> type;
    private final EventProvider<E> provider;
    private final Agent.Factory<E, L> factory;

    public MethodSubscriberAdapter(@NotNull Class<E> type, @NotNull EventProvider<E> provider, @NotNull Agent.Factory<E, L> factory) {
        this.type = type;
        this.provider = provider;
        this.factory = factory;
    }

    public void register(@NotNull L listener) {
        adapt(listener, provider::register);
    }

    @SuppressWarnings("rawtypes")
    public void unregister(@NotNull L listener) {
        provider.unregister(sub -> sub instanceof MethodEventSubscriber && ((MethodEventSubscriber) sub).getListener() == listener);
    }

    @SuppressWarnings("unchecked")
    public void adapt(L listener, BiConsumer<Class<? extends E>, EventSubscriber<E>> consumer) {
        for (Method method : listener.getClass().getDeclaredMethods()) {
            if (!method.isAnnotationPresent(Subscribe.class)) {
                continue;
            }

            if (method.getParameterCount() != 1) {
                throw new SubscriberGenerationException("Subscriber '" + method + "' must have ony one parameter");
            }

            Class<?> methodParameterType = method.getParameterTypes()[0];
            if (!type.isAssignableFrom(methodParameterType)) {
                throw new SubscriberGenerationException("Method paramater '" + methodParameterType + "' does not extend event type '" + type + "'");
            }

            Agent<E, L> executor;
            try {
                executor = factory.create(listener, method);
            } catch(final Exception e) {
                throw new SubscriberGenerationException("Error occurred while creating agent for method '" + method + '\'', e);
            }

            Class<? extends E> clazz = (Class<? extends E>) methodParameterType;
            consumer.accept(clazz, new MethodEventSubscriber<>(clazz, method.getAnnotation(Subscribe.class).priority(), listener, executor));
        }
    }
}