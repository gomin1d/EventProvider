package net.maxim724.event.method;

import net.maxim724.event.DefaultEventProvider;
import net.maxim724.event.api.method.MethodEventProvider;
import net.maxim724.event.method.asm.ASMAgentFactory;
import net.maxim724.event.method.engine.MethodSubscriberAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * <b>Провайдер событий</b>
 * <p>Простая реализация {@link MethodEventProvider}.</p>
 *
 * @param <E> тип события, с которым работает провайдер
 * @param <L> тип слушателя, с которым работает провайдер
 *
 * @since 724.0
 */
public final class DefaultMethodEventProvider<L, E> extends DefaultEventProvider<E> implements MethodEventProvider<L, E> {
    /* Константы */
    public static final MethodEventProvider<Object, Object> COMMON_EVENT_PROVIDER = new DefaultMethodEventProvider<>(Object.class);

    /* Экземпляр */
    private final MethodSubscriberAdapter<L, E> adapter = new MethodSubscriberAdapter<>(type, this, new ASMAgentFactory<>(getClass().getClassLoader()));

    public DefaultMethodEventProvider(Class<E> type) {
        super(Objects.requireNonNull(type, "type"));
    }

    @Override
    public void register(@NotNull L listener) {
        /* Проверяем аргументы методов, которые будут вызваны извне */
        Objects.requireNonNull(listener, "listener");

        adapter.register(listener);
    }

    @Override
    public void unregister(@NotNull L listener) {
        /* Проверяем аргументы методов, которые будут вызваны извне */
        Objects.requireNonNull(listener, "listener");

        adapter.unregister(listener);
    }
}