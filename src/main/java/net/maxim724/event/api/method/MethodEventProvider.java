package net.maxim724.event.api.method;

import net.maxim724.event.api.EventProvider;
import org.jetbrains.annotations.NotNull;

/**
 * <b>Провайдер событий</b>
 * <p>Провайдер событий, способный подписывать методы с аннотацией {@link Subscribe}.</p>
 *
 * @param <L> тип слушателя, с которым будет работать провайдер
 *           <b>Слушатель</b> - класс, который содержит в себе методы с аннотацией {@link Subscribe}.
 * @param <E> тип события, с которым будет работать провайдер
 *
 * @since 724.0
 */
public interface MethodEventProvider<L, E> extends EventProvider<E> {

    /**
     * Зарегистрировать подписчиков слушателя
     * @param listener слушатель
     */
    void register(@NotNull L listener);

    /**
     * Разрегистрировать подписчиков слушателя
     * @param listener слушатель
     */
    void unregister(@NotNull L listener);

}