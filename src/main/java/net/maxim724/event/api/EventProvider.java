package net.maxim724.event.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * <b>Провайдер событий</b>
 * <p>Через него совершаются взаимодействия с подписчиками и событиями.</p>
 *
 * @param <E> тип события, с которым работает провайдер
 *
 * @since 724.0
 */
public interface EventProvider<E> {

    /**
     * Зарегистрировать подписчика
     * @param event событие, на которое подписан подписчик
     * @param subscriber подписчик
     */
    <T extends E> void register(@NotNull Class<T> event, @NotNull EventSubscriber<? super T> subscriber);

    /**
     * Создать и зарегистрировать подписчика
     * @param event событие, на которое подписан подписчик
     * @param priority приоритет подписчика
     * @param consumer логика подписчика
     * @return подписчик
     */
    default <T extends E> @NotNull EventSubscriber<? super T> register(@NotNull Class<T> event, @Nullable Priority priority, Consumer<? super T> consumer) {
        EventSubscriber<? super T> subscriber = new EventSubscriber<T>() {
            @Override
            public void handle(@NotNull T event) {
                consumer.accept(event);
            }

            @Override
            public @NotNull Priority priority() {
                return priority != null ? priority : Priority.NORMAL;
            }
        };

        register(event, subscriber);
        return subscriber;
    }

    /**
     * Опубликовать событие для всех зарегистрированных подписчиков
     * @param event событие
     */
    void publish(@NotNull E event);

    /**
     * Разрегистрировать подписчика
     * @param subscriber подписчик
     */
    void unregister(@NotNull EventSubscriber<?> subscriber);

    /**
     * Разрегистрировать всех подписчиков, удовлетворяющих условие
     * @param predicate условие
     */
    void unregister(@NotNull Predicate<EventSubscriber<?>> predicate);

    /**
     * Разрегистрировать всех подписчиков
     */
    void unregisterAll();

}