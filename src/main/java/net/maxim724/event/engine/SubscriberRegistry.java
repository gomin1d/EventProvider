package net.maxim724.event.engine;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.reflect.TypeToken;
import net.maxim724.event.api.EventSubscriber;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 * <b>Реестр подписчиков</b>
 * <p>Хранилище подписчиков.</p>
 *
 * @param <E> тип события
 *
 * @since 724.0
 */
public final class SubscriberRegistry<E> {
    /* Константы */
    @SuppressWarnings("unchecked")
    private static final LoadingCache<Class<?>, Set<Class<?>>> CLASS_HIERARCHY = CacheBuilder.newBuilder()
            .weakKeys()
            .build(CacheLoader.from(key -> (Set<Class<?>>) TypeToken.of(key).getTypes().rawTypes()));

    /* Экземпляр */
    private final SetMultimap<Class<?>, EventSubscriber<?>> subscribers = LinkedHashMultimap.create();
    private final LoadingCache<Class<?>, List<EventSubscriber<?>>> cache = CacheBuilder.newBuilder()
            .initialCapacity(85)
            .build(CacheLoader.from(eventClass -> {
                List<EventSubscriber<?>> subscribers = new ArrayList<>();
                Set<? extends Class<?>> types = CLASS_HIERARCHY.getUnchecked(eventClass);
                assert types != null;
                synchronized(this.lock) {
                    for(final Class<?> type : types) {
                        subscribers.addAll(this.subscribers.get(type));
                    }
                }

                subscribers.sort(Comparator.comparingInt(s -> s.priority().ordinal()));
                return subscribers;
            }));
    private final Object lock = new Object();

    public <T extends E> void register(@NotNull Class<T> clazz, @NotNull EventSubscriber<? super T> subscriber) {
        synchronized(lock) {
            subscribers.put(clazz, subscriber);
            cache.invalidateAll();
        }
    }

    public void unregister(@NotNull EventSubscriber<?> subscriber) {
        this.unregisterMatching(h -> h.equals(subscriber));
    }

    public void unregisterMatching(@NotNull Predicate<EventSubscriber<?>> predicate) {
        synchronized(lock) {
            boolean dirty = subscribers.values().removeIf(predicate);
            if(dirty) {
                cache.invalidateAll();
            }
        }
    }

    public void unregisterAll() {
        synchronized(lock) {
            subscribers.clear();
            cache.invalidateAll();
        }
    }

    public @NotNull List<EventSubscriber<?>> subscribers(@NotNull Class<?> clazz) {
        return cache.getUnchecked(clazz);
    }
}