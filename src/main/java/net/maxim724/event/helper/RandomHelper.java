package net.maxim724.event.helper;

import java.util.UUID;

public final class RandomHelper {
    private RandomHelper() {
        throw new UnsupportedOperationException();
    }

    public static String generateSession(int length) {
        return UUID.randomUUID().toString().substring(36 - length);
    }
}