package net.maxim724.event.utilty;

import org.jetbrains.annotations.NotNull;

/**
 * Для загрузки собственных классов
 * У {@link ClassLoader} метод {@link ClassLoader#defineClass} является protected
 */
public final class DefiningClassLoader extends ClassLoader {
    public DefiningClassLoader(@NotNull ClassLoader parent) {
        super(parent);
    }

    @SuppressWarnings("unchecked")
    public <T> Class<T> defineClass(@NotNull String name, byte[] bytes) {
        return (Class<T>) defineClass(name, bytes, 0, bytes.length);
    }
}